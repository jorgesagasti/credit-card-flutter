import 'package:flutter/material.dart';
import 'package:flutter_credit_card_new/credit_card_widget.dart';

class CustomCardTypeBackground {
  CustomCardTypeBackground({
    required this.cardType,
    required this.cardAsset,
  });

  CardType cardType;
  String cardAsset;
}
